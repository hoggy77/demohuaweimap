package com.example.demohuawei

import android.Manifest
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.huawei.hms.maps.CameraUpdateFactory
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.OnMapReadyCallback
import com.huawei.hms.maps.model.*
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private val LAT_LNG = LatLng(43.983440, 12.636250)
    private lateinit var rxPermissions: RxPermissions
    private var mapViewBundle: Bundle? = null
    private val MAPVIEW_BUNDLE_KEY = "MapViewBundleKey"
    private var hmap: HuaweiMap? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialize(savedInstanceState)
    }

    private fun initialize(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY)
        }

        rxPermissions = RxPermissions(this)
        mapView.onCreate(mapViewBundle)
        checkPermissions()
    }

    private fun checkPermissions(){
        val request = this.rxPermissions.requestEach(Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION).any { it.granted }

        request.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<Boolean>() {
                override fun onSuccess(granted: Boolean) {
                    if (granted) {
                        mapView.getMapAsync(this@MainActivity)

                    }
                }

                override fun onError(e: Throwable) {
                }
            })

    }

    override fun onMapReady(map : HuaweiMap?) {
        hmap = map!!
        hmap!!.isMyLocationEnabled = true

        // move camera by CameraPosition param ,latlag and zoom params can set here
        val build = CameraPosition.Builder().target(LAT_LNG).zoom(5f).build()

        val cameraUpdate = CameraUpdateFactory.newCameraPosition(build)
        hmap!!.animateCamera(cameraUpdate)
        hmap!!.setMaxZoomPreference(5f)
        hmap!!.setMinZoomPreference(2f)

        // mark can be add by HuaweiMap
        var mMarker = hmap!!.addMarker(
            MarkerOptions().position(LAT_LNG)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.badge_ph))
                .clusterable(true)
        )

        mMarker.showInfoWindow()

        // circle can be add by HuaweiMap
        hmap!!.addCircle(
            CircleOptions().center(
                LAT_LNG
            ).radius(5000.0).fillColor(Color.GREEN)
        )



    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

}
